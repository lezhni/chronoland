<?php

Route::prefix('catalog')->group(function () {
    Route::get('/', 'Site\CatalogController@index')->name('catalog');
    Route::get('{category}', 'Site\CatalogController@category')->name('catalog.category');
    Route::get('{category}/{brand}', 'Site\CatalogController@brand')->name('catalog.brand');
    Route::get('{category}/{brand}/{product}', 'Site\CatalogController@product')->name('catalog.product');
});

Route::prefix('news')->group(function () {
    Route::get('/', 'Site\ArticlesController@articles')->name('news');
    Route::get('{article}', 'Site\ArticlesController@article')->name('news.single');
});

Route::get('/', 'Site\PageController@homepage')->name('home');
//Route::get('{page}', 'Site\PageController@custompage');