$(document).ready(function(){

	$("input[name='phone'], input[type='tel']").mask("+7 (999) 999-99-99");
	
	/* placeholder*/       
    $('input, textarea').each(function(){
        var placeholder = $(this).attr('placeholder');
        $(this).focus(function(){ $(this).attr('placeholder', '');});
        $(this).focusout(function(){             
            $(this).attr('placeholder', placeholder);           
        });
    });
    /* placeholder*/
	
	
	$(".js-popup-close").click(function() {
		$(this).closest(".popup").fadeOut(500);
        $(this).closest(".popup__form").removeClass("active");
        $("body").css("overflow","auto");
		return false;
	});	
	
	$(".js-popup-show").click(function() {
        $("body").css("overflow","hidden");
		var name = $(this).data("popup-name");
		var subj = $(this).data("popup-subj");
		$(".popup."+name).fadeIn(500);
        $("."+name+" .popup__form").addClass("active");
		$(".popup."+name+" .popup__modal input[name='subj']").val(subj);
		return false;
	});
    
    function thanck() {
        $(".popup").fadeOut(500);
        $(".js-popup-thanck").fadeIn(500);
        $(".popup__form").removeClass("active");
        setTimeout('$(".js-popup-thanck").fadeOut(500);',5000);
		$("body").css("overflow","auto");
    }
    
    $(".ajax-form").parsley({
	});

	$(".ajax-form").ajaxForm({
		success: function(){
			thanck();
		}
	});

	/* components */

	if($('.js-slider').length) {
		$('.js-slider').slick({
			arrows: true,
			dots: true,
			infinite: true,
            fade: true,
			speed: 1000,
            animateSlide: false,
			slidesToShow: 1,
			slidesToScroll: 1,
            prevArrow: "<button class='slick-arrow slick-prev js-svg' data-svg-src='/assets/images/icons/arrow_left.svg'></button>",
            nextArrow: "<button class='slick-arrow slick-next js-svg' data-svg-src='/assets/images/icons/arrow_right.svg'></button>",
			responsive: [
				{
				  breakpoint: 479,
				  settings: {
					dots: false,
                      fade: false,
                      speed: 500
				  }
				}			
			]
		});
	};
    
    if($('.cart__gallery-min').length) {
		$('.cart__gallery-min').slick({
			arrows: false,
			dots: false,
			vertical: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            verticalSwiping: true,
            asNavFor: '.cart__gallery-big',
            focusOnSelect: true,
			responsive: [
				{
				  breakpoint: 479,
				  settings: {
					dots: false,
                      fade: false,
                      speed: 500
				  }
				}			
			]
		});
	};
    
    if($('.cart__gallery-big').length) {
		$('.cart__gallery-big').slick({
			arrows: false,
			dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.cart__gallery-min',
			responsive: [
				{
				  breakpoint: 479,
				  settings: {
					dots: false,
                      fade: false,
                      speed: 500
				  }
				}			
			]
		});
	};
    
    if ($(".js-review").length ) {
        $(".js-review").owlCarousel({
            nav:true,
            loop:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                },
                700:{
                    items:2,
                },
                1500:{
                    items:3,
                }
            }
        });
    };
    
    if ($(".js-slider-products").length ) {
        $(".js-slider-products").owlCarousel({
            nav:true,
            loop:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                },
                500:{
                    items:2,
                },
                1025:{
                    items:4,
                },
                1600:{
                    items:5,
                }
            }
        });
    };
	
	$(".js-scroll-to").click(function() {
        var attr_href = $(this).attr("href");
        var data_href = $(this).data("href");
        if ( data_href ) {
            attr_href = data_href;
        }
		$("html, body").animate({
            scrollTop: $(attr_href).offset().top + "px"
        }, {
            duration: 1000
        });
        return false;
    });
	
	$(".js-svg").each(function(){
        var svg_src = $(this).data("svg-src");
        $(this).load(svg_src);
    });
    
    $(".dropdown__head").click(function(){
        
        if ( $(this).closest(".dropdown__item").hasClass("active") ) {
            $(this).closest(".dropdown__item").removeClass("active");
            $(this).closest(".dropdown__item").find(".dropdown__content").slideUp(500);
        } else {
            $(this).closest(".dropdown__item").addClass("active");
            $(this).closest(".dropdown__item").find(".dropdown__content").slideDown(500);
        }
        
    });
    
    $(".contact__tabs-item").click(function(){
        
        var index = $(this).index();
        
        if ( $(this).hasClass("active") ) {
            
        } else {
            $(".contact__tabs-item.active").removeClass("active");
            $(this).addClass("active");
            $(".contact__item:visible").hide();
            $(".contact__item:eq(" + index + ")").fadeIn(500);
        }
        
    });
    
    $(".cart__tabs-item").click(function(){
        
        var index = $(this).index();
        
        if ( $(this).hasClass("active") ) {
            
        } else {
            $(".cart__tabs-item.active").removeClass("active");
            $(this).addClass("active");
            $(".cart-option:visible").hide();
            $(".cart-option:eq(" + index + ")").fadeIn(500);
        }
        
        return false;
        
    });
    
    $(".js-select").click(function(){
        $(this).toggleClass("active");
        $(".catalog__overlay").fadeToggle(500);
    });
    
    $(".catalog__overlay").click(function(){
        $(this).fadeOut(500);
        $(".js-select.active").removeClass("active");
    });
    
    $(".js-select .filter__item").click(function(){
        var this_val = $(this).text();
        $(this).closest(".js-select").find(".filter__input").val( this_val );
    });
    
    $(".popup__brend-item a").click(function(){
        $(this).toggleClass("active");
        return false;
    });
    
    $(".popup__clear").click(function(){
         $(".popup__brend-item a").removeClass("active");
        return false;
    });

});

/* viewport width */
function viewport(){
    var e = window, 
        a = 'inner';
    if ( !( 'innerWidth' in window ) )
    {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */

function animateme() {
    var viewport_height = viewport().height;
    var scroll_top = $(window).scrollTop();
    $(".js-animateme").each(function(){
        var animate_pos = $(this).offset().top;
        var animate_offset = $(this).data("animate-offset");
        var animate_delay = $(this).data("animate-delay");
        var animate = $(this).data("animate-class");
        var win_scroll = scroll_top + viewport_height - animate_offset;
        $(this).css("transition-delay",animate_delay+"ms");
        if ( win_scroll >= animate_pos ) {
            $(this).addClass(animate);
        } else {
            $(this).removeClass(animate);
        }
    });
}

//Анимации по странице
$(window).scroll(function(){
    var viewport_height = viewport().height;
    var viewport_width = viewport().width;
    var scroll_top = $(window).scrollTop();
    
    animateme();
    
    $(".js-paralax").each(function(){
        var paralax_pos = $(this).offset().top;
        var paralax_side = $(this).data("paralax-side");
        var paralax_step = $(this).data("paralax-step");
        if ( paralax_side == 'bottom') {
            $(this).css("margin-bottom", (scroll_top - paralax_pos )/paralax_step );
        } 
        if ( paralax_side == 'left') {
            $(this).css("margin-left", (scroll_top - paralax_pos + viewport_height )/paralax_step );
            if ( viewport_width < viewport_height ) {
                $(this).css("margin-left", (scroll_top - paralax_pos + ( viewport_height/2 ) )/paralax_step );
            }
        } else {
            $(this).css("margin-top", (scroll_top - paralax_pos )/paralax_step );
        }
    });

});