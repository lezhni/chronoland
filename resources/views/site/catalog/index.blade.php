@extends('site.template')

@section('title'){{ $category->title }}@endsection

@section('content')
    <div class="catalog__overlay"></div>
    <div class="catalog">
        <div class="catalog__bg"></div>
        <div class="catalog__container main">
            <h1 class="title catalog__title">{{ $category->title }}</h1>
            <div class="filter">
                <div class="filter__col">
                    <div class="filter__head js-popup-show" data-popup-name="js-popup-brend">
                        <div class="filter__title">Бренд</div>
                        <div class="filter__icon-brend js-svg" data-svg-src="{{ asset('assets/images/icons/brand_icon.svg') }}"></div>
                    </div>
                </div>
                <div class="filter__col js-select">
                    <div class="filter__head">
                        <div class="filter__title">Состояние</div>
                        <div class="filter__icon-down js-svg" data-svg-src="{{ asset('assets/images/icons/select_icon.svg') }}"></div>
                    </div>
                    <div class="filter__list">
                        <a href="#" class="filter__item">Все</a>
                        <a href="#" class="filter__item">Новые</a>
                        <a href="#" class="filter__item">Поддержанные</a>
                    </div>
                    <input type="hidden" class="filter__input">
                </div>
                <div class="filter__col js-select">
                    <div class="filter__head">
                        <div class="filter__title">Кому</div>
                        <div class="filter__icon-down js-svg" data-svg-src="{{ asset('assets/images/icons/select_icon.svg') }}"></div>
                    </div>
                    <div class="filter__list">
                        <a href="#" class="filter__item">Все</a>
                        <a href="#" class="filter__item">Мужские</a>
                        <a href="#" class="filter__item">Женсеикие</a>
                        <a href="#" class="filter__item">Unisex</a>
                    </div>
                    <input type="hidden" class="filter__input">
                </div>
                <div class="filter__col js-select">
                    <div class="filter__head">
                        <div class="filter__title">Форма</div>
                        <div class="filter__icon-down js-svg" data-svg-src="{{ asset('assets/images/icons/select_icon.svg') }}"></div>
                    </div>
                    <div class="filter__list">
                        <a href="#" class="filter__item">Все</a>
                        <a href="#" class="filter__item">Круглые</a>
                        <a href="#" class="filter__item">Квадратные</a>
                        <a href="#" class="filter__item">Прямоугольные</a>
                        <a href="#" class="filter__item">Овал</a>
                        <a href="#" class="filter__item">Бочка</a>
                    </div>
                    <input type="hidden" class="filter__input">
                </div>
                <div class="filter__col js-select">
                    <div class="filter__head">
                        <div class="filter__title">Цена</div>
                        <div class="filter__icon-down js-svg" data-svg-src="{{ asset('assets/images/icons/select_icon.svg') }}"></div>
                    </div>
                    <div class="filter__list">
                        <a href="#" class="filter__item">Любая цена</a>
                        <a href="#" class="filter__item">До 2000$</a>
                        <a href="#" class="filter__item">2000$ - 5000$</a>
                        <a href="#" class="filter__item">5000$ - 10000$</a>
                        <a href="#" class="filter__item">Свыше 10000$</a>
                    </div>
                    <input type="hidden" class="filter__input">
                </div>
                <div class="filter__col filter__col_big">
                    <div class="filter__head">
                        <label class="filter__label" for="check">
                            <input class="filter__checkbox" id="check" type="checkbox" checked>
                            <span class="filter__checkbox-icon"></span>
                            <span class="filter__title">Grand Complication</span>
                        </label>
                    </div>
                </div>
                <div class="filter__col filter__col_min js-select">
                    <div class="filter__head">
                        <div class="filter__title">Цена</div>
                        <div class="filter__icon-down js-svg" data-svg-src="{{ asset('assets/images/icons/restore.svg') }}"></div>
                    </div>
                    <div class="filter__list">
                        <a href="#" class="filter__item">По цене</a>
                        <a href="#" class="filter__item">По алфавиту</a>
                        <a href="#" class="filter__item">По новизне</a>
                    </div>
                    <input type="hidden" class="filter__input">
                </div>
            </div>
            <div class="catalog-list">
                <a href="#" class="catalog-list__item">
                    <span class="catalog-list__img"><img src="{{ asset('assets/images/product1.png') }}" alt=""></span>
                    <span class="catalog-list__title">A. Lange and Sohne</span>
                    <span class="catalog-list__desc">Ultraplate</span>
                    <span class="catalog-list__price">9 800 $</span>
                </a>
                <a href="#" class="catalog-list__item">
                    <span class="catalog-list__img"><img src="{{ asset('assets/images/product2.png') }}" alt=""></span>
                    <span class="catalog-list__title">PATEK PHILIPPE</span>
                    <span class="catalog-list__desc">Ultraplate</span>
                    <span class="catalog-list__price">9 800 $</span>
                </a>
                <a href="#" class="catalog-list__item">
                    <span class="catalog-list__img"><img src="{{ asset('assets/images/product3.png') }}" alt=""></span>
                    <span class="catalog-list__title">BREGUET</span>
                    <span class="catalog-list__desc">Tradition Automatique Seconde Retrograde</span>
                    <span class="catalog-list__price">9 800 $</span>
                </a>
            </div>
            <button class="catalog__more btn btn_stroke">ПОКАЗАТЬ ЕЩЁ</button>
            <div class="catalog__pagin">
                <a href="#" class="catalog__link active">1</a>
                <a href="#" class="catalog__link">2</a>
                <a href="#" class="catalog__link">3</a>
                <a href="#" class="catalog__link">4</a>
                <a href="#" class="catalog__link">5</a>
                <a href="#" class="catalog__link">6</a>
                <a href="#" class="catalog__link">7</a>
                <a href="#" class="catalog__link">...</a>
                <a href="#" class="catalog__link">18</a>
                <a href="#" class="catalog__link">19</a>
            </div>
        </div>
    </div>
    <div class="popup js-popup-brend">
        <div class="popup__overlay js-popup-close"></div>
        <div class="modal-wrapper table">
            <div class="cell">
                <div class="popup__modal popup__brend">
                    <button class="btn btn_green btn_close btn_close-brend js-svg js-popup-close" data-svg-src="/images/icons/close.svg"></button>
                    <div class="popup__brend-list">
                        <div class="popup__brend-row row">
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">A</div>
                                <div class="popup__brend-item"><a href="#">A. Lange and Sohne</a></div>
                                <div class="popup__brend-item"><a href="#">Arnold & Son</a></div>
                                <div class="popup__brend-item"><a href="#">Audemars Piguet</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">B</div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">C</div>
                                <div class="popup__brend-item"><a href="#">Cartier</a></div>
                                <div class="popup__brend-item"><a href="#">Chanel</a></div>
                                <div class="popup__brend-item"><a href="#">Chopard</a></div>
                                <div class="popup__brend-item"><a href="#">CLERC</a></div>
                                <div class="popup__brend-item"><a href="#">Corum</a></div>
                                <div class="popup__brend-item"><a href="#">Cvstos</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">A</div>
                                <div class="popup__brend-item"><a href="#">A. Lange and Sohne</a></div>
                                <div class="popup__brend-item"><a href="#">Arnold & Son</a></div>
                                <div class="popup__brend-item"><a href="#">Audemars Piguet</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">B</div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">C</div>
                                <div class="popup__brend-item"><a href="#">Cartier</a></div>
                                <div class="popup__brend-item"><a href="#">Chanel</a></div>
                                <div class="popup__brend-item"><a href="#">Chopard</a></div>
                                <div class="popup__brend-item"><a href="#">CLERC</a></div>
                                <div class="popup__brend-item"><a href="#">Corum</a></div>
                                <div class="popup__brend-item"><a href="#">Cvstos</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">A</div>
                                <div class="popup__brend-item"><a href="#">A. Lange and Sohne</a></div>
                                <div class="popup__brend-item"><a href="#">Arnold & Son</a></div>
                                <div class="popup__brend-item"><a href="#">Audemars Piguet</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">B</div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">C</div>
                                <div class="popup__brend-item"><a href="#">Cartier</a></div>
                                <div class="popup__brend-item"><a href="#">Chanel</a></div>
                                <div class="popup__brend-item"><a href="#">Chopard</a></div>
                                <div class="popup__brend-item"><a href="#">CLERC</a></div>
                                <div class="popup__brend-item"><a href="#">Corum</a></div>
                                <div class="popup__brend-item"><a href="#">Cvstos</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">A</div>
                                <div class="popup__brend-item"><a href="#">A. Lange and Sohne</a></div>
                                <div class="popup__brend-item"><a href="#">Arnold & Son</a></div>
                                <div class="popup__brend-item"><a href="#">Audemars Piguet</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">B</div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">C</div>
                                <div class="popup__brend-item"><a href="#">Cartier</a></div>
                                <div class="popup__brend-item"><a href="#">Chanel</a></div>
                                <div class="popup__brend-item"><a href="#">Chopard</a></div>
                                <div class="popup__brend-item"><a href="#">CLERC</a></div>
                                <div class="popup__brend-item"><a href="#">Corum</a></div>
                                <div class="popup__brend-item"><a href="#">Cvstos</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">A</div>
                                <div class="popup__brend-item"><a href="#">A. Lange and Sohne</a></div>
                                <div class="popup__brend-item"><a href="#">Arnold & Son</a></div>
                                <div class="popup__brend-item"><a href="#">Audemars Piguet</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">B</div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">C</div>
                                <div class="popup__brend-item"><a href="#">Cartier</a></div>
                                <div class="popup__brend-item"><a href="#">Chanel</a></div>
                                <div class="popup__brend-item"><a href="#">Chopard</a></div>
                                <div class="popup__brend-item"><a href="#">CLERC</a></div>
                                <div class="popup__brend-item"><a href="#">Corum</a></div>
                                <div class="popup__brend-item"><a href="#">Cvstos</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">A</div>
                                <div class="popup__brend-item"><a href="#">A. Lange and Sohne</a></div>
                                <div class="popup__brend-item"><a href="#">Arnold & Son</a></div>
                                <div class="popup__brend-item"><a href="#">Audemars Piguet</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">B</div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">B.R.M.</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                                <div class="popup__brend-item"><a href="#">BOUCHERON</a></div>
                                <div class="popup__brend-item"><a href="#">Blancpain</a></div>
                            </div>
                            <div class="popup__brend-col col-md-2 col-sm-3 col-min-4">
                                <div class="popup__brend-letter">C</div>
                                <div class="popup__brend-item"><a href="#">Cartier</a></div>
                                <div class="popup__brend-item"><a href="#">Chanel</a></div>
                                <div class="popup__brend-item"><a href="#">Chopard</a></div>
                                <div class="popup__brend-item"><a href="#">CLERC</a></div>
                                <div class="popup__brend-item"><a href="#">Corum</a></div>
                                <div class="popup__brend-item"><a href="#">Cvstos</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="popup__brend-bottom">
                        <a href="#" class="popup__clear">
                            <span class="popup__clear-icon js-svg" data-svg-src="/images/icons/clear.svg"></span>
                            <span class="popup__clear-text">сбросить</span>
                        </a>
                        <button class="btn btn_green popup__show">Показать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection