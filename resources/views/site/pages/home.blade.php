@extends('site.template')

@section('content')
    <div class="slider">
        <div class="main">
            <div class="slider__wrap js-slider">
                <div class="slide">
                    <div class="slide__row">
                        <div class="slide__left">
                            <div class="slide__item slide__item_w33">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/2.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                            <div class="slide__item slide__item_w33">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/1.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                            <div class="slide__item slide__item_w33">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/3.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                            <div class="slide__item slide__item_w33">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/5.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                            <div class="slide__item slide__item_w66">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/4.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                        </div>
                        <div class="slide__right slide__item slide__item_big">
                            <div class="slide__top slide__animate">Предложение дня</div>
                            <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/6.png') }}" alt=""></div>
                            <div class="slide__price_left slide__animate">48.995 $</div>
                            <div class="slide__info_right slide__animate">
                                <div class="slide__title_right">PATEK PHILIPPE</div>
                                <div class="slide__desc_right">Cruise collection</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="slide__row">
                        <div class="slide__left">
                            <div class="slide__item slide__item_w33">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/1.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                            <div class="slide__item slide__item_w33">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/1.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                            <div class="slide__item slide__item_w33">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/1.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                            <div class="slide__item slide__item_w33">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/1.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                            <div class="slide__item slide__item_w66">
                                <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/1.png') }}" alt=""></div>
                                <div class="slide__info slide__animate">
                                    <div class="slide__title">Patek hilippe</div>
                                    <div class="slide__desc">Executive Skeleton Tourbillon</div>
                                    <div class="slide__price">12.600 $ </div>
                                </div>
                            </div>
                        </div>
                        <div class="slide__right slide__item slide__item_big">
                            <div class="slide__top slide__animate">Предложение дня</div>
                            <div class="slide__img slide__animate"><img src="{{ asset('assets/images/slider/6.png') }}" alt=""></div>
                            <div class="slide__price_left slide__animate">48.995 $</div>
                            <div class="slide__info_right slide__animate">
                                <div class="slide__title_right">PATEK PHILIPPE</div>
                                <div class="slide__desc_right">Cruise collection</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about">
        <div class="main">
            <img src="{{ asset('assets/images/icons/big_schield.svg') }}" alt="" class="about__logo">
            <div class="mt35">
                <div class="screen-title">Элитный часовой ломбард</div>
            </div>
            <img src="{{ asset('assets/images/logo_big.svg') }}" class="about__title" alt="">
            <div class="about__desc screen-desc">Часовой ломбард Chronoland — место, где вы можете купить <br>или продать качественные брендовые часы, а также получить <br>займ под залог часов или ювелирных украшений</div>
            <div class="about__list">
                <div class="about__item">
                    <div class="about__pic" style="background-image: url({{ asset('assets/images/about/1.jpg') }})"></div>
                </div>
                <div class="about__item">
                    <div class="about__content">
                        <img src="{{ asset('assets/images/icons/lock.svg') }}" alt="" class="about__icon">
                        <div class="about__title1 color-green">безопасность</div>
                        <div class="about__title2">Если вы заложите изделие в наш <br>ломбард, будьте уверены в его <br>сохранности</div>
                        <div class="about__text">Ваши элитные часы б/у под залогом <br>будут храниться в ячейке, в охраняемом <br>помещении. Полная безопасность</div>
                    </div>
                </div>
                <div class="about__item">
                    <div class="about__pic" style="background-image: url({{ asset('assets/images/about/2.jpg') }})"></div>
                </div>
                <div class="about__item">
                    <div class="about__content">
                        <img src="{{ asset('assets/images/icons/lock.svg') }}" alt="" class="about__icon">
                        <div class="about__title1 color-green">конфиденциальность</div>
                        <div class="about__title2">мы гарантируем полную <br>конфеденциальность всех сделок <br>со своими клиентами</div>
                        <div class="about__text">Вы можете быть уверены в том, что все <br>сделаки в Chronoland проходят <br>анонимно. Мы гарантируем полную <br>сохранность всех данных о сделках</div>
                    </div>
                </div>
                <div class="about__item about__item_green">
                    <div class="about__content">
                        <div class="about__text about__text_white">Rolex, Audemars Piguet, Patek <br>PHillipe, A.Lange & Sohne, <br>Blancpain, Breguet, Cartier,<br> Chopard, Corum, Franck Muller, <br>Girard Perregaux, Graham, IWC, <br>Jaeger-LeCoultre, Richard Mille, <br>Vacheron Constantin, Hublot,<br> Greubel Forsey </div>
                    </div>
                </div>
                <div class="about__item">
                    <div class="about__content">
                        <img src="{{ asset('assets/images/icons/lock.svg') }}" alt="" class="about__icon">
                        <div class="about__title1 color-green">широкий ассортимент</div>
                        <div class="about__title2">У нас представлены популярные <br>дорогие модели от именитых <br>швейцарских часовых домов в <br>идеальном техническом состоянии</div>
                        <div class="about__text">Специалисты ломбарда честно <br>и беспристрастно оценивают каждый <br>экземпляр, поэтому вы можете быть <br>уверены: наши цены справедливы </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="servises">
        <div class="main">
            <h2 class="servises__title title">Услуги</h2>
            <div class="servises__list mt40">
                <div class="servises__item">
                    <a href="#" class="servises__thumb"><img src="{{ asset('assets/images/service/1.jpg') }}" alt=""></a>
                    <a href="#" class="servises__link">
                        <span class="servises__link-text">скупка швейцарских часов</span>
                        <span class="servises__link-icon js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
                    </a>
                </div>
                <div class="servises__item">
                    <a href="#" class="servises__thumb"><img src="{{ asset('assets/images/service/2.jpg') }}" alt=""></a>
                    <a href="#" class="servises__link">
                        <span class="servises__link-text">Скупка бриллиантов</span>
                        <span class="servises__link-icon js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
                    </a>
                </div>
                <div class="servises__item servises__item_big">
                    <a href="#" class="servises__thumb"><img src="{{ asset('assets/images/service/3.jpg') }}" alt=""></a>
                    <a href="#" class="servises__link">
                        <span class="servises__link-text">ремонт часов</span>
                        <span class="servises__link-icon js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
                    </a>
                </div>
                <div class="servises__item">
                    <a href="#" class="servises__thumb"><img src="{{ asset('assets/images/service/4.jpg') }}" alt=""></a>
                    <a href="#" class="servises__link">
                        <span class="servises__link-text">кредит под залог часов</span>
                        <span class="servises__link-icon js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
                    </a>
                </div>
                <div class="servises__item">
                    <a href="#" class="servises__thumb"><img src="{{ asset('assets/images/service/5.jpg') }}" alt=""></a>
                    <a href="#" class="servises__link">
                        <span class="servises__link-text">trade-in часов</span>
                        <span class="servises__link-icon js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="online">
        <div class="main">
            <div class="online__wrap">
                <div class="online__head">
                    <h2 class="title">Online-оценка</h2>
                    <div class="mt5">
                        <div class="screen-title color-green">швейцарских часов и ювелирных изделий</div>
                    </div>
                    <img src="{{ asset('assets/images/online.jpg') }}" alt="" class="online__img">
                    <div class="online__desc">Для получения информации о стоимости перед тем, как продать часы, не обязательно лично посещать офис Chronoland. На сайте можно заполнить форму обратной связи для получения экспресс-оценки</div>
                </div>
                <div class="online-form">
                    <div class="online-form__row">
                        <div class="online-form__left">
                            <div class="online-form__time">15</div>
                            <div class="online-form__mins">минут</div>
                            <div class="online-form__line"></div>
                            <div class="online-form__desc">
                                <p>В форме необходимо указать некоторые данные о часах (марка и модель, год покупки, состояние, наличие документов) и приложить несколько
                                    их фото с разных ракурсов. Также необходимо указать контактные данные. Наши специалисты выполнят оценку и при необходимости перезвонят для уточнения деталей.</p>
                                <p>Так клиент сможет узнать примерную сумму, которую можно выручить, если воспользоваться услугой <a href="#">скупки часов.</a></p>
                            </div>
                            <button class="btn btn_grey btn_brend js-popup-show" data-popup-name="js-popup-brend2">
                                <span class="btn__icon-list js-svg" data-svg-src="{{ asset('assets/images/icons/list.svg') }}"></span>
                                список брендов
                            </button>
                        </div>
                        <div class="online-form__right">
                            <h3>Заполните форму</h3>
                            <form action="#" class="form">
                                <input type="text" placeholder="Имя" class="form__input form__input_name error">
                                <input type="tel" placeholder="Телефон" class="form__input form__input_phone">
                                <textarea name="" id="" cols="30" rows="10" class="form__textarea" placeholder="Комментарий"></textarea>
                                <div class="add-foto form__add-foto">
                                    <div class="add-foto__btn js-svg" data-svg-src="{{ asset('assets/images/icons/add.svg') }}"></div>
                                    <div class="add-foto__text">добавить фото</div>
                                    <input type="file" class="add-foto__input">
                                </div>
                                <div class="form__foto-list">
                                    <div class="form__foto-item">
                                        <div class="form__foto-info">
                                            <div class="form__foto-img"><img src="{{ asset('assets/images/service/1.jpg') }}" alt=""></div>
                                            <div class="form__foto-text">picture_name.jpg</div>
                                        </div>
                                        <a href="#" class="form__foto-delete">удалить x</a>
                                    </div>
                                    <div class="form__foto-item">
                                        <div class="form__foto-info">
                                            <div class="form__foto-img"><img src="{{ asset('assets/images/service/1.jpg') }}" alt=""></div>
                                            <div class="form__foto-text">picture_name.jpg</div>
                                        </div>
                                        <a href="#" class="form__foto-delete">удалить x</a>
                                    </div>
                                </div>
                                <button class="btn btn_green form__btn">оценить изделие</button>
                                <div class="form__polity">Нажимая на кнопку, вы даете <a href="#">согласие на обработку</a> своих персональных данных и соглашаетесь с <a href="#">политикой конфиденциальности</a></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-main">
        <div class="main">
            <div class="blog-main__head">
                <h2 class="title">Статьи</h2>
                <div class="blog-main__desc color-green">Chronoland о часах и другом</div>
            </div>
            <div class="post-list">
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
            </div>
            <div class="blog-main__bottom">
                <a href="{{ route('news') }}" class="btn btn_green blog-main__btn">Все статьи</a>
            </div>
        </div>
    </div>
    <div class="popup js-popup-brend2">
        <div class="popup__overlay js-popup-close"></div>
        <div class="modal-wrapper table">
            <div class="cell">
                <div class="popup__modal popup__brend">
                    <button class="btn btn_green btn_close btn_close-brend js-svg js-popup-close" data-svg-src="{{ asset('assets/images/icons/close.svg') }}"></button>
                    <div class="popup__brend-list popup__brend-list_main">
                        <div class="popup__brend2-row row">
                            <div class="col-min-6">
                                <div class="popup__brend2-title">A - G</div>
                                <div class="row">
                                    <div class="col-min-6 popup__brend2-col">
                                        <div class="popup__brend2-item"><a href="#">A. Lange and Sohne</a></div>
                                        <div class="popup__brend2-item"><a href="#">Arnold & Son</a></div>
                                        <div class="popup__brend2-item"><a href="#">Audemars Piguet</a></div>
                                        <div class="popup__brend2-item"><a href="#">B.R.M.</a></div>
                                        <div class="popup__brend2-item"><a href="#">Blancpain</a></div>
                                        <div class="popup__brend2-item"><a href="#">BOUCHERON</a></div>
                                        <div class="popup__brend2-item"><a href="#">Bovet</a></div>
                                        <div class="popup__brend2-item"><a href="#">Breguet</a></div>
                                        <div class="popup__brend2-item"><a href="#">Breitling</a></div>
                                        <div class="popup__brend2-item"><a href="#">Bulgari</a></div>
                                        <div class="popup__brend2-item"><a href="#">Cartier</a></div>
                                        <div class="popup__brend2-item"><a href="#">Chanel</a></div>
                                        <div class="popup__brend2-item"><a href="#">Chopard</a></div>
                                        <div class="popup__brend2-item"><a href="#">CLERC</a></div>
                                        <div class="popup__brend2-item"><a href="#">Corum</a></div>
                                        <div class="popup__brend2-item"><a href="#">Cvstos</a></div>
                                    </div>
                                    <div class="col-min-6 popup__brend2-col">
                                        <div class="popup__brend2-item"><a href="#">DAMIANI</a></div>
                                        <div class="popup__brend2-item"><a href="#">Daniel Roth</a></div>
                                        <div class="popup__brend2-item"><a href="#">De Bethune</a></div>
                                        <div class="popup__brend2-item"><a href="#">de Grisogono</a></div>
                                        <div class="popup__brend2-item"><a href="#">F.P. Journe</a></div>
                                        <div class="popup__brend2-item"><a href="#">Franck Muller</a></div>
                                        <div class="popup__brend2-item"><a href="#">Gerald Genta</a></div>
                                        <div class="popup__brend2-item"><a href="#">Girard-Perregaux</a></div>
                                        <div class="popup__brend2-item"><a href="#">Glashütte Original</a></div>
                                        <div class="popup__brend2-item"><a href="#">Graff</a></div>
                                        <div class="popup__brend2-item"><a href="#">Graham</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-min-6">
                                <div class="popup__brend2-title">H - Z</div>
                                <div class="row">
                                    <div class="col-min-6 popup__brend2-col">
                                        <div class="popup__brend2-item"><a href="#">HARRY WINSTON</a></div>
                                        <div class="popup__brend2-item"><a href="#">Hd3</a></div>
                                        <div class="popup__brend2-item"><a href="#">HUBLOT</a></div>
                                        <div class="popup__brend2-item"><a href="#">HYT</a></div>
                                        <div class="popup__brend2-item"><a href="#">IWC</a></div>
                                        <div class="popup__brend2-item"><a href="#">Jaeger LeCoultre</a></div>
                                        <div class="popup__brend2-item"><a href="#">Jaquet Droz</a></div>
                                        <div class="popup__brend2-item"><a href="#">JORG HYSEK</a></div>
                                        <div class="popup__brend2-item"><a href="#">LOUIS MOINET</a></div>
                                        <div class="popup__brend2-item"><a href="#">Maurice Lacroix</a></div>
                                        <div class="popup__brend2-item"><a href="#">OMEGA</a></div>
                                        <div class="popup__brend2-item"><a href="#">Panerai</a></div>
                                        <div class="popup__brend2-item"><a href="#">Parmigiani</a></div>
                                        <div class="popup__brend2-item"><a href="#">Patek Philippe</a></div>
                                        <div class="popup__brend2-item"><a href="#">Perrelet</a></div>
                                        <div class="popup__brend2-item"><a href="#">Piaget</a></div>
                                    </div>
                                    <div class="col-min-6 popup__brend2-col">
                                        <div class="popup__brend2-item"><a href="#">Roger Dubuis</a></div>
                                        <div class="popup__brend2-item"><a href="#">Rolex</a></div>
                                        <div class="popup__brend2-item"><a href="#">Romain Jerome</a></div>
                                        <div class="popup__brend2-item"><a href="#">TAG Heuer</a></div>
                                        <div class="popup__brend2-item"><a href="#">TUDOR</a></div>
                                        <div class="popup__brend2-item"><a href="#">Ulysse Nardin</a></div>
                                        <div class="popup__brend2-item"><a href="#">Urwerk</a></div>
                                        <div class="popup__brend2-item"><a href="#">Vacheron Constantin</a></div>
                                        <div class="popup__brend2-item"><a href="#">Zenith</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="popup__brend-bottom popup__brend-bottom_end">
                        <button class="btn btn_green popup__show js-popup-close">Понятно</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection