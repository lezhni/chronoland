@extends('site.template')

@section('content')
    <div class="page">
        <div class="main">
            <div class="page__head">
                <h2 class="title">Статьи</h2>
                <div class="page__desc color-green">chronoland о часах и другом</div>
            </div>
            <div class="post-list">
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
            </div>
            <div class="blog-main__bottom">
                <button class="btn btn_green blog-main__btn">Показать ещё</button>
            </div>
        </div>
    </div>
@endsection