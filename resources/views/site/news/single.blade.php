@extends('site.template')

@section('content')
    <div class="page">
        <div class="main">
            <div class="page__head">
                <h2 class="title">Статьи</h2>
                <div class="page__desc color-green">Chronoland о часах и другом</div>
            </div>
            <div class="single">
                <div class="single__date">1 октября 2018</div>
                <h3 class="single__desc">Часы Rolex Day-date стали самым дорогим лотом <br>престижного аукциона Antiquorum</h3>
                <div class="single__preview">
                    <img src="{{ asset('assets/images/monday.jpg') }}" alt="">
                </div>
                <div class="single__content">
                    <h5>Первый в истории водонипроницаемый хронограф</h5>
                    <p>Часы Day-Date с автоматическим подзаводом, созданные в 1956 году, стали первым в истории водонепроницаемым хронометром, оснащенным современным календарем, мгновенно показывающим полное название дней недели и дату в специальных окошках циферблата. Точность, надежность, отличная читаемость показаний и непревзойденный респектабельный дизайн этой престижной модели позволяют часам Day-Date соответствовать самому высокому статусу их обладателя.</p>
                    <p><img src="{{ asset('assets/images/VIII.jpg') }}" alt=""></p>
                    <h5>Первый в истории водонипроницаемый хронограф</h5>
                    <p>А также акционеры крупнейших компаний, превозмогая сложившуюся непростую экономическую ситуацию, разоблачены. Задача организации, в особенности же высококачественный прототип будущего проекта предопределяет высокую востребованность поэтапного и последовательного развития общества. Прежде всего, разбавленное изрядной долей эмпатии, рациональное мышление способствует повышению качества инновационных методов управления процессами. Лишь независимые государства являются только методом политического участия и ассоциативно распределены по отраслям. Учитывая ключевые сценарии поведения, постоянный количественный рост и сфера нашей активности не даёт нам иного выбора, кроме определения экономической целесообразности принимаемых решений.</p>
                    <p><img src="{{ asset('assets/images/clock.jpg') }}" alt=""></p>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-main blog-main_single">
        <div class="main">
            <div class="blog-main__head">
                <h2 class="title">Другие статьи</h2>
            </div>
            <div class="post-list">
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
                <a href="#" class="post-list__item">
                    <span class="post-list__thumb"><img src="{{ asset('assets/images/post_thumb1.jpg') }}" alt=""></span>
                    <span class="post-list__title">Как писал Иван Айвазовский</span>
                    <span class="post-list__desc">Хорошие швейцарские часы с репетиром — вещь дорогая. Но это настоящий показатель статуса. Выражение “часы с репетиром” наверняка приходилось слышать многим. Но что это за часы? Что такое репетир и зачем он нужен?</span>
                    <span class="post-list__bottom">
	                    <span class="post-list__date">21 февраля</span>
	                    <span class="post-list__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></span>
	                </span>
                </a>
            </div>
            <div class="blog-main__bottom">
                <a href="{{ route('news') }}" class="btn btn_green blog-main__btn">Все статьи</a>
            </div>
        </div>
    </div>
@endsection