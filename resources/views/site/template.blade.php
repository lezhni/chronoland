<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="keywords" content="@yield('keywords')">
    <meta name="keywords" content="@yield('description')">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <link rel="stylesheet" href="{{ mix('assets/css/style.min.css') }}">
</head>
<body>
<div class="main-wrapper">
    <header class="header">
        <div class="main">
            <div class="header__row">
                <div class="header__left">
                    <a href="{{ route('home')  }}" class="header__logo"><img src="{{ asset('assets/images/logo.svg') }}" alt="Chronoland"></a>
                    <nav class="header__catalog">
                        <a href="#" class="header__catalog-item">Швейцарские часы</a>
                        <a href="#" class="header__catalog-item">ювелирные украшения</a>
                        <a href="#" class="header__catalog-item">Аксессуары</a>
                        <a href="#" class="header__catalog-item">Новинки 2019</a>
                    </nav>
                </div>
                <div class="header__right">
                    <nav class="header__menu">
                        <a href="#" class="header__menu-item">оценить</a>
                        <a href="#" class="header__menu-item">продать</a>
                        <a href="#" class="header__menu-item">заложить</a>
                        <a href="#" class="header__menu-item">контакты</a>
                    </nav>
                    <div class="header__phone-icon js-svg js-popup-show" data-popup-name="js-popup-form" data-svg-src="{{ asset('assets/images/icons/phone.svg') }}"></div>
                    <a href="tel:+74953638356" class="header__phone-number">+7 495 363 83 56</a>
                    <a href="#" class="header-bottom__stock">
                        <span class="header-bottom__stock-icon js-svg" data-svg-src="{{ asset('assets/images/icons/buy_stock.svg') }}"></span>
                        <span class="header-bottom__stock-count">0</span>
                    </a>
                    <div class="header__burger js-popup-show" data-popup-name="js-popup-menu">
                        <div class="header__burger-line"></div>
                        <div class="header__burger-line"></div>
                        <div class="header__burger-line"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="header-bottom">
        <div class="main">
            <div class="header-bottom__row">
                <div class="header-bottom__left">
                    <div class="select-city">
                        <select name="city" id="" class="select-city__item">
                            <option><a href="#">Москва</a></option>
                            <option><a href="#">Санкт-Петербург</a></option>
                        </select>
                        <div class="select-city__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_down.svg') }}"></div>
                    </div>
                    <div class="header-bottom__currency">
                        <a href="#" class="header-bottom__currency-item">Рубли</a>
                        <a href="#" class="header-bottom__currency-item active">Доллары</a>
                        <a href="#" class="header-bottom__currency-item">Евро</a>
                    </div>
                </div>
                <div class="header-bottom__right">
                    <div class="header-bottom__mess">мессенджеры</div>
                    <div class="header-bottom__phone-number">+7 926 679-99-99</div>
                    <div class="header-bottom__line"></div>
                    <a href="#" class="header-bottom__phone-icon js-svg js-popup-show" data-popup-name="js-popup-form" data-svg-src="{{ asset('assets/images/icons/phone.svg') }}"></a>
                    <a href="#" class="header-bottom__call js-popup-show" data-popup-name="js-popup-form">Заказать звонок</a>
                </div>
                <div class="header-bottom__right2">
                    <nav class="header-bottom__menu">
                        <a href="#" class="header-bottom__menu-item">оценить</a>
                        <a href="#" class="header-bottom__menu-item">продать</a>
                        <a href="#" class="header-bottom__menu-item">заложить</a>
                        <a href="#" class="header-bottom__menu-item">контакты</a>
                    </nav>
                    <a href="#" class="header-bottom__stock">
                        <span class="header-bottom__stock-icon js-svg" data-svg-src="{{ asset('assets/images/icons/buy_stock.svg') }}"></span>
                        <span class="header-bottom__stock-count">0</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    @yield('content')

    <div class="search-form">
        <div class="main">
            <form action="#" class="search-form__wrap">
                <button class="btn btn_green search-form__btn js-svg" data-svg-src="{{ asset('assets/images/icons/search.svg') }}"></button>
                <input type="text" class="search-form__input" placeholder="Поиск по каталогу">
                <button class="search-form__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></button>
            </form>
        </div>
    </div>
    <footer class="footer">
        <div class="main">
            <div class="footer__wrap row">
                <div class="col-sm-7 col-lg-6">
                    <div class="footer__adress">Москва, <br>Кутузовский проспект <br>дом 26, корпус 1</div>
                    <a href="#" class="btn btn_stroke footer__open-map">открыть в Яндекс.картах</a>
                </div>
                <div class="col-sm-3 col-lg-2">
                    <div class="footer__menu">
                        <a href="#" class="footer__menu-item">Швейцарские часы</a>
                        <a href="#" class="footer__menu-item">Ювелирные изделия</a>
                        <a href="#" class="footer__menu-item">Аксессуары</a>
                        <a href="#" class="footer__menu-item">Новинки 2019</a>
                    </div>
                </div>
                <div class="col-sm-2 col-lg-1">
                    <div class="footer__menu footer__menu_right">
                        <a href="#" class="footer__menu-item">Продать</a>
                        <a href="#" class="footer__menu-item">Оценить</a>
                        <a href="#" class="footer__menu-item">Заложить</a>
                        <a href="#" class="footer__menu-item">Контакты</a>
                    </div>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-sm-12 col-lg-2">
                    <div class="row">
                        <div class="col-sm-7 col-lg-12">
                            <div class="footer__about">
                                <a href="#" class="footer__about-item">О ломбарде</a>
                                <a href="#" class="footer__about-item">Гарантии</a>
                            </div>
                        </div>
                        <div class="col-sm-5 col-lg-12">
                            <form action="#" class="subscribe">
                                <input type="text" class="subscribe__input" placeholder="Подписка на новости">
                                <button class="subscribe__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_next.svg') }}"></button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer__bottom col-sm-3">
                    <div class="footer__label color-green">Whatsapp, Viber, Telegram</div>
                    <div class="footer__contact">+7 926 679 99 99</div>
                </div>
                <div class="footer__bottom col-sm-3">
                    <div class="footer__label color-green">Городской телефон</div>
                    <div class="footer__contact">+7 495 363 83 56</div>
                </div>
                <div class="footer__bottom col-sm-4">
                    <div class="footer__label color-green">Почта</div>
                    <div class="footer__contact">info@chronoland.ru</div>
                </div>
                <div class="footer__bottom col-sm-2">
                    <a href="#" class="footer__develop">
                        <span class="footer__develop-text">Сайт <br>разработан</span>
                        <span class="footer__develop-logo js-svg" data-svg-src="{{ asset('assets/images/all.svg') }}"></span>
                    </a>
                </div>
            </div>
        </div>
    </footer>
    <div class="popup js-popup-form">
        <div class="popup__overlay js-popup-close"></div>
        <div class="popup__form table">
            <button class="btn btn_green btn_close js-svg js-popup-close" data-svg-src="{{ asset('assets/images/icons/close.svg') }}"></button>
            <div class="table__cell">
                <form action="#" class="popup__form-box">
                    <div class="popup__form-title">Обратный звонок</div>
                    <input type="text" placeholder="Имя" class="form__input form__input_name">
                    <input type="tel" placeholder="Телефон" class="form__input form__input_phone">
                    <button class="btn btn_green form__btn">Перезвоните мне</button>
                    <div class="form__polity">Нажимая на кнопку, вы даете <a href="#">согласие на обработку</a> своих персональных данных и соглашаетесь с <a href="#">политикой конфиденциальности</a></div>
                </form>
            </div>
        </div>
    </div>
    <div class="popup popup_menu js-popup-menu">
        <div class="main">
            <div class="popup__head">
                <div class="select-city">
                    <select name="city" class="select-city__item">
                        <option><a href="#">Москва</a></option>
                        <option><a href="#">Санкт-Петербург</a></option>
                    </select>
                    <div class="select-city__arrow js-svg" data-svg-src="{{ asset('assets/images/icons/arrow_down.svg') }}"></div>
                </div>
                <div class="popup__head-line"></div>
                <a href="#" class="header-bottom__stock">
                    <span class="header-bottom__stock-icon js-svg" data-svg-src="{{ asset('assets/images/icons/buy_stock.svg') }}"></span>
                    <span class="header-bottom__stock-count">0</span>
                    <span class="header-bottom__stock-text">Избранное</span>
                </a>
                <div class="popup__close popup__close_menu js-popup-close js-svg" data-svg-src="{{ asset('assets/images/icons/close.svg') }}"></div>
            </div>
            <div class="modal-menu">
                <div class="modal-menu__wrap">
                    <a href="{{ route('home') }}" class="modal-menu__logo"><img src="{{ asset('assets/images/logo_big.svg') }}" alt="Chronoland"></a>
                    <div class="modal-menu__row row">
                        <div class="modal-menu__col col-md-3 col-min-6">
                            <h5 class="modal-menu__title">Каталог</h5>
                            <nav class="modal-menu__list">
                                <a href="#" class="modal-menu__item">Швейцарские часы</a>
                                <a href="#" class="modal-menu__item">Ювелирные украшения</a>
                                <a href="#" class="modal-menu__item">Аксессуары</a>
                                <a href="#" class="modal-menu__item">Новинки 2019</a>
                            </nav>
                        </div>
                        <div class="modal-menu__col col-md-3 col-min-6">
                            <h5 class="modal-menu__title">Услуги</h5>
                            <nav class="modal-menu__list">
                                <a href="#" class="modal-menu__item">Продать часы</a>
                                <a href="#" class="modal-menu__item">Продать бриллианты</a>
                                <a href="#" class="modal-menu__item">Кредит под залог часов</a>
                                <a href="#" class="modal-menu__item">Trade-in часов</a>
                                <a href="#" class="modal-menu__item">Ремонт часов</a>
                                <a href="#" class="modal-menu__item">Online-оценка</a>
                            </nav>
                        </div>
                        <div class="modal-menu__col col-md-3 col-min-6">
                            <h5 class="modal-menu__title">О ломбарде</h5>
                            <nav class="modal-menu__list">
                                <a href="#" class="modal-menu__item">Общая информация</a>
                                <a href="#" class="modal-menu__item">Гарантии</a>
                                <a href="#" class="modal-menu__item">Отзывы</a>
                                <a href="#" class="modal-menu__item">Документы</a>
                                <a href="#" class="modal-menu__item">Вопрос-ответ</a>
                            </nav>
                        </div>
                        <div class="modal-menu__col col-md-3 col-min-6">
                            <h5 class="modal-menu__title">Контакты</h5>
                            <nav class="modal-menu__list">
                                <a href="#" class="modal-menu__item">Москва</a>
                                <a href="#" class="modal-menu__item">Санкт-Петербург</a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup__bottom">
                <a href="tel:+74953638356" class="popup__phone">
                    <span class="popup__phone-icon js-svg" data-svg-src="{{ asset('assets/images/icons/phone.svg') }}"></span>
                    <span class="popup__phone-number">+7 495 363 83 56</span>
                </a>
                <div class="popup__adress">
                    <div class="popup__adress-icon js-svg" data-svg-src="{{ asset('assets/images/icons/location.svg') }}"></div>
                    <div class="popup__adress-text">Кутузовский проспект, д. 26, к. 1</div>
                </div>
                <a href="#" class="popup__polity">Политика конфиденциальности</a>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="{{ mix('assets/js/scripts.min.js') }}"></script>
</body>
</html>