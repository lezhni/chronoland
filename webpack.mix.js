const mix = require('laravel-mix');

mix
    .sass('resources/assets/scss/style.scss', 'public/assets/css/style.min.css')
    .options({ processCssUrls: false })
    .scripts(['resources/assets/js/components/*.js', 'resources/assets/js/data.js'], 'public/assets/js/scripts.min.js')
    .copy('resources/assets/fonts', 'public/assets/fonts')
    .copy('resources/assets/images', 'public/assets/images')
    .version();
