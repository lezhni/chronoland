<?php

namespace App\Console\Commands;

use App\Services\ExchangeRatesService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * Class UpdateExchangeRates
 * @package App\Console\Commands
 */
class UpdateExchangeRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchange-rates:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update exchange rates';

    /**
     * Execute the console command.
     *
     * @param \App\Services\ExchangeRatesService $exchangeRatesService
     * @return mixed
     */
    public function handle(ExchangeRatesService $exchangeRatesService)
    {
        try {
            $newExchangeRates = $exchangeRatesService->getRemote();
        } catch (GuzzleException $e) {
            $this->processError($e);
            return;
        } catch (Throwable $e) {
            $this->processError($e);
            return;
        }

        try {
            $exchangeRatesService->store($newExchangeRates);
        } catch (Throwable $e) {
            $this->processError($e);
            return;
        }

        $this->info('Exchange rates successfully updated.');
        return;
    }

    /**
     * @param \GuzzleHttp\Exception\GuzzleException|\Throwable $exception
     */
    protected function processError($exception): void
    {
        $error = $exception->getMessage();
        Log::error("Error while updating exchange rates: $error");
        $this->error('Can\'t update exchange rates, check logs for details.');
    }
}
