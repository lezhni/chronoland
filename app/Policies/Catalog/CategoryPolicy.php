<?php

namespace App\Policies\Catalog;

use App\Models\User;
use App\Models\Catalog\Category;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class CategoryPolicy
 * @package App\Policies\Catalog
 */
class CategoryPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any categories.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the category.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Catalog\Category $category
     * @return mixed
     */
    public function view(User $user, Category $category)
    {
        return true;
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Catalog\Category $category
     * @return mixed
     */
    public function update(User $user, Category $category)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Catalog\Category $category
     * @return mixed
     */
    public function delete(User $user, Category $category)
    {
        return false;
    }
}
