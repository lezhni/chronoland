<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class ExchangeRatesService
 * @package App\Services
 */
class ExchangeRatesService
{
    /**
     * @var string
     */
    private const CBR_API_URL = 'http://cbr.ru/scripts/XML_daily.asp';

    /**
     * @var array
     */
    private const CURRENCIES_CODES = ['USD', 'EUR'];

    /**
     * Get exchange rates from settings.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function get(): array
    {
        $siteSettingsFile = config('nova-settings-tool.filename');
        $siteSettings = Storage::get($siteSettingsFile);
        $siteSettings = json_decode($siteSettings, true);

        $exchangeRates = [];
        foreach (self::CURRENCIES_CODES as $currency) {
            $key = 'exchange_rate_' . strtolower($currency);
            if ( array_key_exists($key, $siteSettings)) {
                $exchangeRates[$currency] = [
                    'code' => $currency,
                    'value' => (double) $siteSettings[$key],
                ];
            }
        }

        return $exchangeRates;
    }

    /**
     * Get new exchange rates from remote host.
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function getRemote(): array
    {
        $currentDate = Carbon::tomorrow()->format('d/m/Y');

        $client = new Client();
        $response = $client->request('GET', self::CBR_API_URL, [
            'query' => [ 'date_req' => $currentDate ],
            'allow_redirects' => false,
        ]);

        $statusCode = $response->getStatusCode();
        if ($statusCode !== 200) {
            $reason = $response->getReasonPhrase();
            throw new Exception($reason, $statusCode);
        }

        $content = $response->getBody()->getContents();
        $allExchangeRates = simplexml_load_string($content);
        if ($allExchangeRates === false) {
            throw new Exception('CBR API response\'s body isn\'t XML string', 400);
        }

        $exchangeRates = [];
        foreach ($allExchangeRates->Valute as $currency) {
            if (in_array($currency->CharCode, self::CURRENCIES_CODES)) {
                $code = (string) $currency->CharCode;
                $value  = (double) str_replace(',', '.', $currency->Value);
                $value = $value / (int) $currency->Nominal;

                $exchangeRates[$code] = [
                    'code' => $code,
                    'value' => $value,
                ];
            }
        }

        return $exchangeRates;
    }

    /**
     * Store new exchange rates in settings.
     *
     * @param array $exchangeRates
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function store(array $exchangeRates): void
    {
        $siteSettingsFile = config('nova-settings-tool.filename');
        $siteSettings = Storage::disk('local')->get($siteSettingsFile);
        $siteSettings = json_decode($siteSettings, true);

        foreach (self::CURRENCIES_CODES as $currency) {
            if ( array_key_exists($currency, $exchangeRates)) {
                $key = 'exchange_rate_' . strtolower($currency);
                $siteSettings[$key] = $exchangeRates[$currency]['value'];
            }
        }

        $siteSettings = json_encode($siteSettings);
        Storage::put($siteSettingsFile, $siteSettings);
    }
}