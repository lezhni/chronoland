<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function homepage()
    {
        return view('site.pages.home');
    }

    public function custompage()
    {
    }
}