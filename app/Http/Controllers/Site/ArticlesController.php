<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{
    public function articles()
    {
        return view('site.news.index');
    }

    public function article()
    {
        return view('site.news.single');
    }
}
