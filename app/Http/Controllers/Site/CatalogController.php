<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Catalog\Brand;
use App\Models\Catalog\Category;
use Exception;

class CatalogController extends Controller
{
    public function index()
    {
        try {
            $defaultCategory = Category::firstOrFail();
        } catch (Exception $e) {
            abort(404);
        }

        return redirect()->route('catalog.category', $defaultCategory->alias);
    }

    public function category(Category $category)
    {
        return view('site.catalog.index', compact('category'));
    }

    public function brand(Category $category, Brand $brand)
    {
    }

    public function product()
    {
    }
}