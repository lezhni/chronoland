<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Settings Path
    |--------------------------------------------------------------------------
    |
    | Path to the JSON file where settings are stored.
    |
    */

    'filename' => 'settings.json',

    'path' => storage_path('app/settings.json'),

    /*
    |--------------------------------------------------------------------------
    | Sidebar Label
    |--------------------------------------------------------------------------
    |
    | The text that Nova displays for this tool in the navigation sidebar.
    |
    */

    'sidebar-label' => 'Настройки сайта',

    /*
    |--------------------------------------------------------------------------
    | Settings
    |--------------------------------------------------------------------------
    |
    | The good stuff :). Each setting defined here will render a field in the
    | tool. The only required key is `key`, other available keys include `type`,
    | `label`, `help`, `placeholder`, `language`, and `panel`.
    |
    */

    'settings' => [
        [
            'key' => 'exchange_rate_usd',
            'type' => 'number',
            'label' => 'Курс USD / RUB',
            'panel' => 'Курсы валют',
            'help' => 'Курс валют автоматически обновляется каждый день согласно данным Центробанка РФ',
        ],
        [
            'key' => 'exchange_rate_eur',
            'type' => 'number',
            'label' => 'Курс EUR / RUB',
            'panel' => 'Курсы валют',
            'help' => 'Курс валют автоматически обновляется каждый день согласно данным Центробанка РФ',
        ]
    ],

];
