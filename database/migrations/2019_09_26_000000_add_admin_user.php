<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;

class AddAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = new User();
        $user->name = 'Администратор';
        $user->email = 'admin@chronoland.ru';
        $user->password = Hash::make('administrator');
        $user->is_admin = true;
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws \Exception
     */
    public function down()
    {
        User::find(1)->delete();
    }
}
