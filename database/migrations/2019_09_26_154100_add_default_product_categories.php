<?php

use App\Models\Catalog\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Carbon;

class AddDefaultProductCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = Carbon::now();
        Category::insert([
            [
                'title' => 'Швейцарские часы',
                'alias' => 'chasy',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Ювелирные украшение',
                'alias' => 'juvelirnye_izdeliya',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Аксессуары',
                'alias' => 'aksessuary',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('alias', ['chasy', 'juvelirnye_izdeliya', 'aksessuary'])->delete();
    }
}
